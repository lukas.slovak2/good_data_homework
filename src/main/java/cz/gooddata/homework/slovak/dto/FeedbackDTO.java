package cz.gooddata.homework.slovak.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;

/**
 * DTO used for providing saved data to user
 *
 * @author Lukas Slovak
 */
@JsonPropertyOrder({"id", "name", "message", "insertionTime"})
public class FeedbackDTO extends FeedbackInputDTO {

    private Long id;
    private Date insertionTime;

    public FeedbackDTO(Long id, String name, String message, Date insertionTime) {
        super(name, message);
        this.id = id;
        this.insertionTime = insertionTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getInsertionTime() {
        return insertionTime;
    }

    public void setInsertionTime(Date insertionTime) {
        this.insertionTime = insertionTime;
    }
}
