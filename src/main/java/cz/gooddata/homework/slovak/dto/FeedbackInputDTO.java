package cz.gooddata.homework.slovak.dto;

/**
 * DTO used for working with user's input
 *
 * @author Lukas Slovak
 */
public class FeedbackInputDTO {

    private String name;
    private String message;

    //for jackson
    protected FeedbackInputDTO(){
    }

    public FeedbackInputDTO(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
