package cz.gooddata.homework.slovak.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author Lukas Slovak
 */
@Entity
public class Feedback {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date insertionTime;

    private String name;

    private String message;

    // for jpa use
    protected Feedback() {
    }

    public Feedback(String name, String message) {
        this.name = name;
        this.message = message;
        this.insertionTime = new Date();
    }

    public Date getInsertionTime() {
        return insertionTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", insertionTime=" + insertionTime +
                ", name='" + name + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
