package cz.gooddata.homework.slovak.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception that is thrown when user post feedback with empty message
 *
 * @author Lukas Slovak
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmptyMessageException extends RuntimeException{

    public EmptyMessageException() {
        super("Message has to be specified.");
    }

}
