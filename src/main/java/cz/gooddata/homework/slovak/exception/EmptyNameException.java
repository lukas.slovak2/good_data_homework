package cz.gooddata.homework.slovak.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception that is thrown when user sends feedback with empty name
 *
 * @author Lukas Slovak
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmptyNameException extends RuntimeException {

    public EmptyNameException(){
        super("Name has to be specified.");
    }

}
