package cz.gooddata.homework.slovak.repository;

import cz.gooddata.homework.slovak.entity.Feedback;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Lukas Slovak
 */
public interface FeedbackRepository extends CrudRepository<Feedback, Long> {

    List<Feedback> findByName(String name);

}
