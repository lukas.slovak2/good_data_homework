package cz.gooddata.homework.slovak.rest;

import cz.gooddata.homework.slovak.dto.FeedbackDTO;
import cz.gooddata.homework.slovak.dto.FeedbackInputDTO;
import cz.gooddata.homework.slovak.service.IFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lukas Slovak
 */
@RestController
@RequestMapping("/feedback")
public class FeedbackRestController {

    @Autowired
    private IFeedbackService feedbackService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public FeedbackDTO add( @RequestBody FeedbackInputDTO feedbackDTO){
        return feedbackService.add(feedbackDTO);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<FeedbackDTO> getAll(){
        return feedbackService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{name}")
    public List<FeedbackDTO> get(@PathVariable String name){
        return feedbackService.get(name);
    }

}
