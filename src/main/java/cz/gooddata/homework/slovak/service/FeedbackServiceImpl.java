package cz.gooddata.homework.slovak.service;

import cz.gooddata.homework.slovak.dto.FeedbackDTO;
import cz.gooddata.homework.slovak.dto.FeedbackInputDTO;
import cz.gooddata.homework.slovak.entity.Feedback;
import cz.gooddata.homework.slovak.exception.EmptyMessageException;
import cz.gooddata.homework.slovak.exception.EmptyNameException;
import cz.gooddata.homework.slovak.repository.FeedbackRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service used for the work with feedback
 *
 * @author Lukas Slovak
 */
@Service
public class FeedbackServiceImpl implements IFeedbackService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackServiceImpl.class);

    @Autowired
    private FeedbackRepository repository;

    @Override
    public List<FeedbackDTO> getAll() {
        final Iterable<Feedback> allFeedback = repository.findAll();
        return mapToDTO(allFeedback);
    }

    @Override
    public List<FeedbackDTO> get(String name) {
        //find feedback for given name
        final List<Feedback> nameFeedBack = repository.findByName(name);
        return mapToDTO(nameFeedBack);
    }

    /**
     * validates input ( checks if the fields 'name' and 'message' are filled), then creates new {@link Feedback} entity and save it into database
     *
     * @param feedbackDTO
     * @return
     */
    @Override
    public FeedbackDTO add(FeedbackInputDTO feedbackDTO) {
        //validate request
        validate(feedbackDTO);
        //save request to DB
        final Feedback savedFeedback = repository.save(new Feedback(feedbackDTO.getName(), feedbackDTO.getMessage()));
        LOGGER.info("Feedback successfully saved " + savedFeedback);
        return new FeedbackDTO(savedFeedback.getId(), savedFeedback.getName(), savedFeedback.getMessage(), savedFeedback.getInsertionTime());
    }

    /**
     * maps {@link Feedback} entities to DTOs
     *
     * @param allFeedback
     * @return
     */
    private List<FeedbackDTO> mapToDTO(Iterable<Feedback> allFeedback) {
        List<FeedbackDTO> resultList = new ArrayList<>();
        allFeedback.forEach(f -> resultList.add(new FeedbackDTO(f.getId(), f.getName(), f.getMessage(), f.getInsertionTime())));
        return resultList;
    }

    /**
     * checks if the 'name' and 'message' fields are non-empty
     *
     * @param feedbackDTO
     */
    private void validate(FeedbackInputDTO feedbackDTO) {
        if (feedbackDTO.getName() == null || feedbackDTO.getName().isEmpty()) {
            throw new EmptyNameException();
        }

        if (feedbackDTO.getMessage() == null || feedbackDTO.getMessage().isEmpty()) {
            throw new EmptyMessageException();
        }
    }
}
