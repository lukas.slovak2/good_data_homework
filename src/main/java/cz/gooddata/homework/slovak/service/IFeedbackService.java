package cz.gooddata.homework.slovak.service;

import cz.gooddata.homework.slovak.dto.FeedbackDTO;
import cz.gooddata.homework.slovak.dto.FeedbackInputDTO;

import java.util.List;

/**
 * @author Lukas Slovak
 */
public interface IFeedbackService {

    List<FeedbackDTO> getAll();

    List<FeedbackDTO> get(String name);

    FeedbackDTO add(FeedbackInputDTO feedbackDTO);

}
