package cz.gooddata.homework.slovak.service;

import cz.gooddata.homework.slovak.dto.FeedbackDTO;
import cz.gooddata.homework.slovak.dto.FeedbackInputDTO;
import cz.gooddata.homework.slovak.entity.Feedback;
import cz.gooddata.homework.slovak.exception.EmptyMessageException;
import cz.gooddata.homework.slovak.exception.EmptyNameException;
import cz.gooddata.homework.slovak.repository.FeedbackRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

/**
 * @author Lukas Slovak
 */
@RunWith(MockitoJUnitRunner.class)
public class FeedbackServiceTest {

    private static final String TEST_NAME_1 = "test1";
    private static final String TEST_NAME_2 = "test2";
    private static final String TEST_MESSAGE_1 = "message1";
    private static final String TEST_MESSAGE_2 = "message2";

    @InjectMocks
    private FeedbackServiceImpl feedbackService;

    @Mock
    private FeedbackRepository repository;

    @Test
    public void getAll() {
        Mockito.when(repository.findAll()).thenReturn(Arrays.asList(new Feedback(TEST_NAME_1, TEST_MESSAGE_1), new Feedback(TEST_NAME_2, TEST_MESSAGE_2)));

        final List<FeedbackDTO> foundFeedback = feedbackService.getAll();

        Assert.assertNotNull(foundFeedback);
        Assert.assertEquals(2, foundFeedback.size());
        Assert.assertEquals(TEST_NAME_1, foundFeedback.get(0).getName());
        Assert.assertEquals(TEST_MESSAGE_1, foundFeedback.get(0).getMessage());
        Assert.assertEquals(TEST_NAME_2, foundFeedback.get(1).getName());
        Assert.assertEquals(TEST_MESSAGE_2, foundFeedback.get(1).getMessage());
    }

    @Test
    public void get() {
        Mockito.when(repository.findByName(TEST_NAME_1)).thenReturn(Arrays.asList(new Feedback(TEST_NAME_1, TEST_MESSAGE_1)));
        Mockito.when(repository.findByName(TEST_NAME_2)).thenReturn(Arrays.asList(new Feedback(TEST_NAME_2, TEST_MESSAGE_2)));

        final List<FeedbackDTO> test1Feedback = feedbackService.get(TEST_NAME_1);
        Assert.assertNotNull(test1Feedback);
        Assert.assertEquals(1, test1Feedback.size());
        Assert.assertEquals(TEST_NAME_1, test1Feedback.get(0).getName());
        Assert.assertEquals(TEST_MESSAGE_1, test1Feedback.get(0).getMessage());

        final List<FeedbackDTO> test2Feedback = feedbackService.get(TEST_NAME_2);
        Assert.assertNotNull(test2Feedback);
        Assert.assertEquals(1, test2Feedback.size());
        Assert.assertEquals(TEST_NAME_2, test2Feedback.get(0).getName());
        Assert.assertEquals(TEST_MESSAGE_2, test2Feedback.get(0).getMessage());
    }

    @Test
    public void add() {
        Feedback savedFeedback = new Feedback(TEST_NAME_1, TEST_MESSAGE_1);
        Mockito.when(repository.save(Matchers.any(Feedback.class))).thenReturn(savedFeedback);

        final FeedbackDTO addedFeedback = feedbackService.add(new FeedbackInputDTO(TEST_NAME_1, TEST_MESSAGE_1));
        Assert.assertNotNull(addedFeedback);
        Assert.assertEquals(savedFeedback.getInsertionTime(), addedFeedback.getInsertionTime());
        Assert.assertEquals(savedFeedback.getName(), addedFeedback.getName());
        Assert.assertEquals(savedFeedback.getMessage(), addedFeedback.getMessage());
    }

    @Test(expected = EmptyNameException.class)
    public void addEmptyName() {
        feedbackService.add(new FeedbackInputDTO("", TEST_MESSAGE_1));
    }

    @Test(expected = EmptyNameException.class)
    public void addNullName() {
        feedbackService.add(new FeedbackInputDTO(null, TEST_MESSAGE_1));
    }

    @Test(expected = EmptyMessageException.class)
    public void addEmptyMessage() {
        feedbackService.add(new FeedbackInputDTO(TEST_NAME_1, ""));
    }

    @Test(expected = EmptyMessageException.class)
    public void addNullMessage() {
        feedbackService.add(new FeedbackInputDTO(TEST_NAME_1, null));
    }

}
